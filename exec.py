import selenium
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from bs4 import BeautifulSoup
import time

class haz_sites:

    def __init__(self):
        self.driver = webdriver.Firefox()
        self.driver.maximize_window()

    def make_soup_obj(self):
        self.soup_obj = BeautifulSoup(self.driver.page_source, "lxml")

    def get_to_haz_start_page(self):
        self.driver.delete_all_cookies()
        self.driver.get('https://anrweb.vt.gov/DEC/ERT/Hazsites.aspx')

    def get_town_list(self):
        self.driver.find_element_by_xpath('//a[@id="body_lbListTowns"]').click()
        self.make_soup_obj()
        select_tag = self.soup_obj.find('select', {'id' : 'body_ddlTown'})
        town_list = []

        for option_tag in select_tag.findChildren('option')[1:]:
            town_list.append(option_tag.text)

        return town_list

    def get_to_town_page(self, town):
        self.driver.find_element_by_xpath('//input[@id="body_txtSiteTown"]').send_keys(town)
        self.driver.find_element_by_xpath('//input[@id="body_btnSearch"]').click()


    def get_site_no(self):
        table_tag = self.soup_obj.find('table', {'id' : 'body_gvSites'})
        tr_tags = table_tag.findChildren('tr')[1:]
        site_nos = []

        for tr in tr_tags:
            site_nos.append(tr.findChildren('td')[1].text)

        return site_nos

    def get_to_site_page(self, no):
        self.get_to_haz_start_page()
        self.driver.find_element_by_xpath('//input[@id="body_txtSiteNumber"]').send_keys(no)
        self.driver.find_element_by_xpath('//input[@id="body_btnSearch"]').click()
        time.sleep(2)
        self.driver.find_element_by_xpath('//input[@type="button"]').click()

    def parse_site_page(self):
        table_tag = self.soup_obj.find('table', {'id' : 'body_dvSite'})
        to_ret = []

        for tr_tags in table_tag.findChildren('tr'):
            td_tags = tr_tags.findChildren('td')
            to_ret.append(td_tags[1].text)


        report_list = []

        try:
            table_tag = self.soup_obj.find('table', {'id' : 'body_gvReports'})
            tr_tags = table_tag.findChildren('tr')[1:]

            for tr in tr_tags:
                a_tag = tr.findChild('a')
                report_list.append([a_tag.text, a_tag['href']])
        except:
            pass

        relationshiop_list = []

        try:
            table_tag = self.soup_obj.find('table', {'id' : 'body_gvMILO'})
            tr_tags = table_tag.findChildren('tr')[1:]
            for tr in tr_tags:
                td_tags = tr.findChildren('td')
                link = td_tags[0].findChild('a')['href']
                site_type = td_tags[1].text
                id1 = td_tags[2].text
                id2 = td_tags[3].text
                staff = td_tags[4].text
                relationshiop_list.append([site_type, id1, id2, staff, link])

        except:
            pass

        return to_ret, report_list, relationshiop_list


    def tear_down(self):
        self.driver.quit()

class brownfield_sites:

    def __init__(self):
        self.driver = webdriver.Firefox()
        self.driver.maximize_window()

    def make_soup_obj(self):
        self.soup_obj = BeautifulSoup(self.driver.page_source, "lxml")

    def get_to_brownfield_start_page(self):
        self.driver.delete_all_cookies()
        self.driver.get('https://anrweb.vt.gov/DEC/ERT/Brownfields.aspx')

    def get_priority_list(self):
        self.make_soup_obj()
        select_tag = self.soup_obj.find('select', {'id' : 'body_ddlPriority'})
        priority_list = []

        for option_tag in select_tag.findChildren('option')[1:]:
            priority_list.append(option_tag.text)

        return priority_list

    def get_to_priority_page(self, priority):
        self.driver.find_element_by_xpath('//select[@id="body_ddlPriority"]/option[text()="{}"]'.format(priority)).click()
        self.driver.find_element_by_xpath('//input[@id="body_btnSearch"]').click()


    def get_site_no(self):
        site_nos = []

        try:
            table_tag = self.soup_obj.find('table', {'id' : 'body_gvSites'})
            tr_tags = table_tag.findChildren('tr')[1:]

            for tr in tr_tags:
                site_nos.append(tr.findChildren('td')[1].text)

        except:
            pass

        return site_nos


    def get_to_site_page(self, no):
        self.get_to_brownfield_start_page()
        self.driver.find_element_by_xpath('//input[@id="body_txtSiteNumber"]').send_keys(no)
        self.driver.find_element_by_xpath('//input[@id="body_btnSearch"]').click()
        time.sleep(2)
        self.make_soup_obj()

        table_tag = self.soup_obj.find('table', {'id' : 'body_gvSites'})
        tr = table_tag.findChild('tr').findNext('tr')
        td_tags = tr.findChildren('td')
        county = td_tags[4].text

        self.driver.find_element_by_xpath('//input[@type="button"]').click()

        return county

    def get_site_no(self):
        table_tag = self.soup_obj.find('table', {'id' : 'body_gvSites'})
        tr_tags = table_tag.findChildren('tr')[1:]
        site_nos = []

        for tr in tr_tags:
            site_nos.append(tr.findChildren('td')[1].text)

        return site_nos


    def parse_site_page(self):
        table_tag = self.soup_obj.find('table', {'id' : 'body_dvSite'})
        to_ret = []

        for tr_tags in table_tag.findChildren('tr'):
            td_tags = tr_tags.findChildren('td')
            to_ret.append(td_tags[1].text)

        return to_ret


class USTs:

    def __init__(self):
        self.driver = webdriver.Firefox()
        self.driver.maximize_window()

    def make_soup_obj(self):
        self.soup_obj = BeautifulSoup(self.driver.page_source, "lxml")

    def get_to_UST_start_page(self):
        self.driver.delete_all_cookies()
        self.driver.get('https://anrweb.vt.gov/DEC/ERT/UST2.aspx')


    def get_to_town_page(self, town):
        self.driver.find_element_by_xpath('//input[@id="body_txtTown"]').send_keys(town)
        self.driver.find_element_by_xpath('//input[@id="body_btnSearch"]').click()

    def get_site_no(self):
        site_nos = []

        try:
            table_tag = self.soup_obj.find('table', {'id' : 'body_gvSites'})
            tr_tags = table_tag.findChildren('tr')[1:]

            for tr in tr_tags:
                site_nos.append(tr.findChildren('td')[1].text)

        except:
            pass

        return site_nos


    def get_to_site_page(self, no):
        self.get_to_UST_start_page()
        self.driver.find_element_by_xpath('//input[@id="body_txtFacilityID"]').send_keys(no)
        self.driver.find_element_by_xpath('//input[@id="body_btnSearch"]').click()
        time.sleep(2)
        self.driver.find_element_by_xpath('//input[@type="button"]').click()

    def get_site_no(self):
        table_tag = self.soup_obj.find('table', {'id' : 'body_gvSites'})
        tr_tags = table_tag.findChildren('tr')[1:]
        site_nos = []

        for tr in tr_tags:
            site_nos.append(tr.findChildren('td')[1].text)

        return site_nos


    def parse_site_page(self):
        table_tag = self.soup_obj.find('table', {'id' : 'body_dvSite'})
        to_ret = []

        for tr_tags in table_tag.findChildren('tr'):
            td_tags = tr_tags.findChildren('td')
            to_ret.append(td_tags[1].text)

        table_tag = self.soup_obj.find('table', {'id' : 'body_gvTanks'})
        tanks = []

        for tr_tag in table_tag.findChildren('tr')[1:]:
            temp_list = []
            for td_tag in tr_tag.findChildren('td'):
                temp_list.append(td_tag.text)
            tanks.append(temp_list)
        return to_ret, tanks


class haz_waste:

    def __init__(self):
        self.driver = webdriver.Firefox()
        self.driver.maximize_window()

    def make_soup_obj(self):
        self.soup_obj = BeautifulSoup(self.driver.page_source, "lxml")

    def get_to_haz_waste_start_page(self):
        self.driver.delete_all_cookies()
        self.driver.get('https://anrweb.vt.gov/DEC/ERT/Manifests.aspx')


    def get_to_town_page(self, town):
        self.driver.find_element_by_xpath('//input[@id="body_txtTown"]').send_keys(town)
        self.driver.find_element_by_xpath('//input[@id="body_btnSearch"]').click()

    def get_site_no(self):
        site_nos = []

        try:
            table_tag = self.soup_obj.find('table', {'id' : 'body_gvSites'})
            tr_tags = table_tag.findChildren('tr')[1:]

            for tr in tr_tags:
                site_nos.append(tr.findChildren('td')[1].text)

        except:
            pass

        return site_nos


    def get_to_site_page(self, no):
        self.get_to_haz_waste_start_page()
        self.driver.find_element_by_xpath('//input[@id="body_txtGeneratorID"]').send_keys(no)
        self.driver.find_element_by_xpath('//input[@id="body_btnSearch"]').click()
        time.sleep(2)
        self.driver.find_element_by_xpath('//input[@type="button"]').click()

    def get_site_no(self):
        table_tag = self.soup_obj.find('table', {'id' : 'body_gvSites'})
        tr_tags = table_tag.findChildren('tr')[1:]
        site_nos = []

        for tr in tr_tags:
            site_nos.append(tr.findChildren('td')[1].text)

        return site_nos


    def parse_site_page(self):
        table_tag = self.soup_obj.find('table', {'id' : 'body_dvSite'})
        to_ret = []

        for tr_tags in table_tag.findChildren('tr'):
            td_tags = tr_tags.findChildren('td')
            to_ret.append(td_tags[1].text)

        return to_ret

class solid_waste:

    def __init__(self):
        self.driver = webdriver.Firefox()
        self.driver.maximize_window()

    def make_soup_obj(self):
        self.soup_obj = BeautifulSoup(self.driver.page_source, "lxml")

    def get_to_solid_waste_start_page(self):
        self.driver.delete_all_cookies()
        self.driver.get('https://anrweb.vt.gov/DEC/ERT/SolidWaste.aspx')


    def get_to_town_page(self, town):
        self.driver.find_element_by_xpath('//input[@id="body_txtTown"]').send_keys(town)
        self.driver.find_element_by_xpath('//input[@id="body_btnSearch"]').click()

    def get_site_no(self):
        site_nos = []

        try:
            table_tag = self.soup_obj.find('table', {'id' : 'body_gvSites'})
            tr_tags = table_tag.findChildren('tr')[1:]

            for tr in tr_tags:
                site_nos.append(tr.findChildren('td')[1].text)

        except:
            pass

        return site_nos


    def get_to_site_page(self, no):
        self.get_to_solid_waste_start_page()
        self.driver.find_element_by_xpath('//input[@id="body_txtfacilityid"]').send_keys(no)
        self.driver.find_element_by_xpath('//input[@id="body_btnSearch"]').click()
        time.sleep(2)
        self.driver.find_element_by_xpath('//input[@type="button"]').click()

    def get_site_no(self):
        table_tag = self.soup_obj.find('table', {'id' : 'body_gvSites'})
        tr_tags = table_tag.findChildren('tr')[1:]
        site_nos = []

        for tr in tr_tags:
            site_nos.append(tr.findChildren('td')[1].text)

        return site_nos


    def parse_site_page(self):
        table_tag = self.soup_obj.find('table', {'id' : 'body_dvSite'})
        to_ret = []

        for tr_tags in table_tag.findChildren('tr'):
            td_tags = tr_tags.findChildren('td')
            to_ret.append(td_tags[1].text)

        return to_ret

    def driver_page_source(self):
        return self.driver.page_source

class ASTs:

    def __init__(self):
        self.driver = webdriver.Firefox()
        self.driver.maximize_window()

    def make_soup_obj(self):
        self.soup_obj = BeautifulSoup(self.driver.page_source, "lxml")

    def get_to_ast_start_page(self):
        self.driver.delete_all_cookies()
        self.driver.get('https://anrweb.vt.gov/DEC/ERT/AST.aspx')


    def open_site_page(self):
        self.driver.find_element_by_xpath('//input[@id="body_txtFacilityID"]').send_keys('%')
        self.driver.find_element_by_xpath('//input[@id="body_btnSearch"]').click()

    def get_site_no(self):
        site_nos = []

        try:
            table_tag = self.soup_obj.find('table', {'id' : 'body_gvSites'})
            tr_tags = table_tag.findChildren('tr')[1:]

            for tr in tr_tags:
                site_nos.append(tr.findChildren('td')[1].text)

        except:
            pass

        return site_nos


    def get_to_site_page(self, no):
        self.get_to_ast_start_page()
        self.driver.find_element_by_xpath('//input[@id="body_txtFacilityID"]').send_keys(no)
        self.driver.find_element_by_xpath('//input[@id="body_btnSearch"]').click()
        time.sleep(2)
        self.driver.find_element_by_xpath('//input[@type="button"]').click()

    def parse_site_page(self):
        table_tag = self.soup_obj.find('table', {'id' : 'body_dvSite'})
        to_ret = []

        for tr_tags in table_tag.findChildren('tr'):
            td_tags = tr_tags.findChildren('td')
            to_ret.append(td_tags[1].text)

        return to_ret

    def driver_page_source(self):
        return self.driver.page_source

    def tear_down(self):
        self.driver.quit()


class DryCleaners:

    def __init__(self):
        self.driver = webdriver.Firefox()
        self.driver.maximize_window()

    def make_soup_obj(self):
        self.soup_obj = BeautifulSoup(self.driver.page_source, "lxml")

    def get_to_dry_cleaners_start_page(self):
        self.driver.delete_all_cookies()
        self.driver.get('https://anrweb.vt.gov/DEC/ERT/DryCleaners.aspx')


    def open_site_page(self):
        self.driver.find_element_by_xpath('//input[@id="body_txtName"]').send_keys('%')
        self.driver.find_element_by_xpath('//input[@id="body_btnSearch"]').click()
        time.sleep(2)

    def get_site_name_address(self):
        site_name_address = []

        table_tag = self.soup_obj.find('table', {'id' : 'body_gvSites'})
        tr_tags = table_tag.findChildren('tr')[1:]

        for tr in tr_tags:
            site_name_address.append([tr.findChildren('td')[2].text, tr.findChildren('td')[3].text])

        return site_name_address


    def get_to_site_page(self, name, address):
        self.get_to_dry_cleaners_start_page()
        self.driver.find_element_by_xpath('//input[@id="body_txtName"]').send_keys(name)
        self.driver.find_element_by_xpath('//input[@id="body_txtAddress"]').send_keys(address)
        self.driver.find_element_by_xpath('//input[@id="body_btnSearch"]').click()
        time.sleep(2)
        self.driver.find_element_by_xpath('//input[@type="button"]').click()

    def parse_site_page(self):
        table_tag = self.soup_obj.find('table', {'id' : 'body_dvSite'})
        to_ret = []

        for tr_tags in table_tag.findChildren('tr'):
            td_tags = tr_tags.findChildren('td')
            to_ret.append(td_tags[1].text)

        return to_ret

    def driver_page_source(self):
        return self.driver.page_source

    def tear_down(self):
        self.driver.quit()

class salvage_yard:

    def __init__(self):
        self.driver = webdriver.Firefox()
        self.driver.maximize_window()

    def make_soup_obj(self):
        self.soup_obj = BeautifulSoup(self.driver.page_source, "lxml")

    def get_to_salvage_yards_start_page(self):
        self.driver.delete_all_cookies()
        self.driver.get('https://anrweb.vt.gov/DEC/ERT/SalvageYards.aspx')

    def open_town_page(self, town):
        self.driver.find_element_by_xpath('//input[@id="body_txtTownName"]').send_keys(town)
        self.driver.find_element_by_xpath('//input[@id="body_btnSearch"]').click()

    def make_soup_obj(self):
        self.soup_obj = BeautifulSoup(self.driver.page_source, "lxml")

    def parse_town_page(self):

        table_tag = self.soup_obj.find('table', {'id' : 'body_gvSites'})
        if table_tag == None:
            return []
        to_ret = []
        for tr_tag in table_tag.findChildren('tr')[1:]:
            td_tags = tr_tag.findChildren('td')[1:]
            temp_list = []
            for td in td_tags:
                temp_list.append(td.text)
            to_ret.append(temp_list)

        return to_ret

    def tear_down(self):
        self.driver.quit()

class spill:

    def __init__(self):
        self.driver = webdriver.Firefox()
        self.driver.maximize_window()

    def make_soup_obj(self):
        self.soup_obj = BeautifulSoup(self.driver.page_source, "lxml")

    def get_to_spill_start_page(self):
        self.driver.delete_all_cookies()
        self.driver.get('https://anrweb.vt.gov/DEC/ERT/Spills.aspx')

    def get_to_year_page(self, start_date, end_date):
        print(start_date)
        print(end_date)
        self.driver.delete_all_cookies()
        self.driver.find_element_by_xpath('//input[@id="body_txtBeginDate"]').clear()
        self.driver.find_element_by_xpath('//input[@id="body_txtEndDate"]').clear()
        self.driver.find_element_by_xpath('//input[@id="body_txtBeginDate"]').send_keys(start_date)
        self.driver.find_element_by_xpath('//input[@id="body_txtEndDate"]').send_keys(end_date)
        self.driver.find_element_by_xpath('//input[@id="body_btnSearch"]').click()

    def make_soup_obj(self):
        self.soup_obj = BeautifulSoup(self.driver.page_source, "lxml")

    def parse_year_page(self):

        table_tag = self.soup_obj.find('table', {'id' : 'body_gvSites'})
        to_ret = []

        for tr_tag in table_tag.findChildren('tr')[1:]:
            td_tags = tr_tag.findChildren('td')[1:]
            temp_list = []
            for td in td_tags:
                temp_list.append(td.text)
            to_ret.append(temp_list)

        return to_ret

def hazards():
    h = haz_sites()
    h.get_to_haz_start_page()
    town_list = h.get_town_list()
    site_no = []
    for town in town_list:
        h.get_to_haz_start_page()
        h.get_to_town_page(town)
        h.make_soup_obj()
        site_no = h.get_site_no()
        for no in site_no:
            h.get_to_site_page(no)
            h.make_soup_obj()
            site_nos, reports, relationships = h.parse_site_page()
    h.tear_down()

def brownfield():
    b = brownfield_sites()
    b.get_to_brownfield_start_page()
    priority_list = b.get_priority_list()
    site_no = []
    for priority in priority_list:
        b.get_to_brownfield_start_page()
        b.get_to_priority_page(priority)
        b.make_soup_obj()
        site_no = b.get_site_no()
        for no in site_no:
            county = b.get_to_site_page(no)
            b.make_soup_obj()
            site_nos = b.parse_site_page()
            print(site_nos)
    b.tear_down()

def UST_method():
    u = USTs()
    town_list = ['Addison', 'Albany', 'Alburgh', 'Andover']
    site_no = []
    for town in town_list:
        u.get_to_UST_start_page()
        u.get_to_town_page(town)
        u.make_soup_obj()
        site_no = u.get_site_no()
        for no in site_no:
            u.get_to_site_page(no)
            u.make_soup_obj()
            site_nos, tanks = u.parse_site_page()
            print(site_nos)
            print(tanks)

    u.tear_down()

def haz_waste_method():
    h = haz_waste()
    town_list = ['Addison', 'Albany', 'Alburgh', 'Andover']
    site_no = []
    for town in town_list:
        h.get_to_haz_waste_start_page()
        h.get_to_town_page(town)
        h.make_soup_obj()
        site_no = h.get_site_no()
        for no in site_no:
            h.get_to_site_page(no)
            h.make_soup_obj()
            site_nos = h.parse_site_page()
            print(site_nos)

    h.tear_down()

def solid_waste_method():
    s = solid_waste()
    town_list = ['Andover']
    site_no = []
    for town in town_list:
        s.get_to_solid_waste_start_page()
        s.get_to_town_page(town)
        s.make_soup_obj()
        try:
            site_no = s.get_site_no()
        except:
            continue
        for no in site_no:
            s.get_to_site_page(no)
            s.make_soup_obj()
            site_nos = s.parse_site_page()
            print(site_nos)

    s.tear_down()

def ast_method():
    a = ASTs()
    a.get_to_ast_start_page()
    a.open_site_page()
    a.make_soup_obj()
    site_nos = a.get_site_no()
    site_nos = []
    for site in site_nos:
        a.get_to_ast_start_page()
        a.get_to_site_page(site)
        a.make_soup_obj()
        site_nos = a.parse_site_page()
        print(site_nos)
    a.tear_down()

def dry_celarners_method():
    d = DryCleaners()
    d.get_to_dry_cleaners_start_page()
    d.open_site_page()
    d.make_soup_obj()
    site_name_address = d.get_site_name_address()
    print(site_name_address)
    site_nos = []
    for site in site_name_address:
        d.get_to_dry_cleaners_start_page()
        d.get_to_site_page(site[0], site[1])
        d.make_soup_obj()
        site_nos = d.parse_site_page()
        print(site_nos)

    d.tear_down()

def salvage_yard_method():
    s = salvage_yard()
    s.get_to_salvage_yards_start_page()
    town_list = ['Addison', 'Albany', 'Alburgh']
    for town in town_list:
        s.get_to_salvage_yards_start_page()
        s.open_town_page(town)
        s.make_soup_obj()
        temp_list = s.parse_town_page()
        print(temp_list)

    s.tear_down()

def spill_method():
    s = spill()
    s.get_to_spill_start_page()
    for year in range(1973, 2019):
        s.get_to_spill_start_page()
        s.get_to_year_page('1/1/{}'.format(year), '12/31/{}'.format(year))
        s.make_soup_obj()
        spills = s.parse_year_page()
        print(spills)

if __name__ == '__main__':

